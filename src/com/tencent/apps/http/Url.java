package com.tencent.apps.http;
/**
 * 一定要加网络权限
 * @author cui
 *
 */
public class Url {
	//服务器地址
	public static String SERVER_HOST = "http://127.0.0.1:8090/";
	
	//首页的地址 http://127.0.0.1:8090/home?index=0
	public static String HOME = SERVER_HOST+ "home?index=";//因为是一次取20条数据，此处index=0不对，应把0去掉
	//首页图片的前缀
	public static String HOME_IMAGE= SERVER_HOST+"image?name=";
	//应用接口    
	public static String HOME_APPS= SERVER_HOST+"app?index=";
	//游戏接口
	public static String HOME_GAME= SERVER_HOST+"game?index=";
	//专题接口 
	public static String HOME_SUBJECT= SERVER_HOST+"subject?index=";
	//推荐接口
	public static String HOME_RECOMMEND= SERVER_HOST+"recommend?index=";
	//分类接口
	public static String HOME_CATEGORY= SERVER_HOST+"category?index=";
	//热门接口
	public static String HOME_HOT= SERVER_HOST+"hot?index=";
	//应用详情接口
	public static String HOME_DETAIL= SERVER_HOST+"detail?packageName=";
	//下载接口
	public static String HOME_DOWNLOAD= SERVER_HOST+"download?name=downloadUrl";
	//断点下载接口
	public static String HOME_DOWNLOAD2= SERVER_HOST+"download?name=downloadUrl&range=672121";
}
