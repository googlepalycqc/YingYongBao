package com.tencent.apps.ui.activity;

import java.util.ArrayList;

import com.tencent.apps.R;
import com.tencent.apps.lib.photoview.HackyViewPager;
import com.tencent.apps.ui.adapter.ImageScaleAdapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
//import android.support.v4.view.ViewPager;
import android.widget.HorizontalScrollView;

public class ImageScaleActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_scale);
		//data
		Intent intent = getIntent();
		ArrayList<String> screenList = intent.getStringArrayListExtra("screenList");
		int currentNum = intent.getIntExtra("currentNum", 0);//???
		
		//用HackyViewPager不用ViewPager，是解决：IllegalArgumentException: pointerIndex out of range.
		HackyViewPager viewpager = (HackyViewPager) findViewById(R.id.viewpager);
		viewpager.setPadding(0, 0, 0, 0);
		ImageScaleAdapter adapter = new ImageScaleAdapter(this, screenList, currentNum);
		viewpager.setAdapter(adapter);
		viewpager.setCurrentItem(currentNum);
	}
}
