package com.tencent.apps.ui.activity;

import com.lidroid.xutils.util.LogUtils;
import com.tencent.apps.R;
import com.tencent.apps.bean.AppInfo;
import com.tencent.apps.global.AppsApplication;
import com.tencent.apps.http.HttpHelper;
import com.tencent.apps.http.Url;
import com.tencent.apps.ui.fragment.ContentPager;
import com.tencent.apps.ui.part.AppInfoDesPart;
import com.tencent.apps.ui.part.AppInfoPart;
import com.tencent.apps.ui.part.AppInfoSafePart;
import com.tencent.apps.ui.part.ImageScalePart;
import com.tencent.apps.utils.CommonUtil;
import com.tencent.apps.utils.GsonUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


public class AppDetailActivity  extends ActionBarActivity{
		
	private String packageName;
	public Context context = this;
	private AppInfo appInfo;
	private AppInfoPart appInfoPart;
	private AppInfoSafePart appInfoSafePart;
	private ImageScalePart imageScalPart;
	private AppInfoDesPart appInfoDesPart;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		packageName = intent.getExtras().getString("packageName");
		ContentPager contentPager = new ContentPager(this) {
			

			@Override
			protected Object loadData() {
				return requestData();
			}
			
			@Override
			protected View createView() {
				View view = View.inflate(getContext(), R.layout.activity_appdetail, null);//context
				ScrollView scrollView = (ScrollView) view.findViewById(R.id.scrollView);
				LinearLayout layout_content = (LinearLayout) view.findViewById(R.id.layout_content);
				LinearLayout layout_bottom = (LinearLayout) view.findViewById(R.id.layout_bottom);
				//模块一
				appInfoPart = new AppInfoPart();
				View view1 = appInfoPart.getView();
				layout_content.addView(view1);
				
				appInfoSafePart = new AppInfoSafePart();
				View view2 = appInfoSafePart.getView();
				layout_content.addView(view2);
				
				imageScalPart = new ImageScalePart(context);
				View view3 = imageScalPart.getView();
				layout_content.addView(view3);
				
				appInfoDesPart = new AppInfoDesPart(scrollView);
				View view4 = appInfoDesPart.getView();
				layout_content.addView(view4);
				return view;
			}
		};
		setContentView(contentPager);
		
		setActionBar();
	}

	private void setActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		return super.onOptionsItemSelected(item);
	}
	protected Object requestData() {
		String url = Url.HOME_DETAIL + packageName;
		String json = HttpHelper.get(url);
		appInfo = GsonUtil.jsonToBean(json, AppInfo.class);
		if (appInfo != null) {
			CommonUtil.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					appInfoPart.setView(appInfo);
					appInfoSafePart.setView(appInfo);
					imageScalPart.setView(appInfo);
					appInfoDesPart.setView(appInfo);
				}
			});
		}
		
		return appInfo;
	}
}
