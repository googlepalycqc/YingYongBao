package com.tencent.apps.ui.activity;

import com.tencent.apps.R;
import com.tencent.apps.R.layout;
import com.tencent.apps.R.menu;
import com.tencent.apps.lib.PagerSlidingTab;
import com.tencent.apps.ui.adapter.MainViewPagerAdapter;
import com.tencent.apps.utils.CommonUtil;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends ActionBarActivity {

	private DrawerLayout drawerLayout;
	private PagerSlidingTab pagerSlidingTab;
	private ViewPager viewPager;
	private ActionBarDrawerToggle drawerToggle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initView();
		initActionBar();
	}
/**
 * 5点：拿到actionbar,基本设置+设置home图标+设置三条线+drawerLayout设置监听+重写方法onOptionsItemSelected*/
	private void initActionBar() {
		ActionBar actionBar = getSupportActionBar();
		//下面2行不写也一样因为不用也可以，ic_launcher+app_name字段名称跟默认的一样。
//		actionBar.setIcon(R.drawable.ic_launcher);//
//		actionBar.setTitle(R.string.app_name);//
		
		actionBar.setDisplayHomeAsUpEnabled(true);//是否有home左侧的侧拉图标
		actionBar.setDisplayShowHomeEnabled(true);//是否显示home图标
		
		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer_am, 0, 0);
		drawerToggle.syncState();
		
		drawerLayout.setDrawerListener(new DrawerListener() {
			
			@Override
			public void onDrawerStateChanged(int newState) {
				drawerToggle.onDrawerStateChanged(newState);
			}
			
			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				drawerToggle.onDrawerSlide(drawerView, slideOffset);
			}
			
			@Override
			public void onDrawerOpened(View drawerView) {
				drawerToggle.onDrawerOpened(drawerView);
			}
			
			@Override
			public void onDrawerClosed(View drawerView) {
				drawerToggle.onDrawerClosed(drawerView);
			}
		});
	}

	private void initView() {
		drawerLayout = (DrawerLayout) findViewById(R.id.drawLayout);
		pagerSlidingTab = (PagerSlidingTab) findViewById(R.id.pagerSlidingTab);
		viewPager = (ViewPager) findViewById(R.id.viewPager);
		
		viewPager.setAdapter(new MainViewPagerAdapter(getSupportFragmentManager()));
		pagerSlidingTab.setViewPager(viewPager);
//		viewPager.setOffscreenPageLimit(CommonUtil.getStringArray(R.array.tab_names).length);//预加载几个页面
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		drawerToggle.onOptionsItemSelected(item);
		return super.onOptionsItemSelected(item);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	

}
