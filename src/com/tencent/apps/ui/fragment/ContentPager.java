package com.tencent.apps.ui.fragment;

import java.util.List;

import com.tencent.apps.R;
import com.tencent.apps.lib.PagerSlidingTab;
import com.tencent.apps.utils.CommonUtil;
import com.tencent.apps.utils.LogUtil;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public abstract class ContentPager extends FrameLayout {
	

	private static final String TAG = "ContentPager";

	public ContentPager(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initContentPager();
	}

	public ContentPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		initContentPager();
	}

	public ContentPager(Context context) {
		super(context);
		initContentPager();
	}

	// 定义4种加载状态对应的view，然后根据状态让对应的view显示出来
	private View loadingView;
	private View successView;
	private View errorView;
	private View emptyView;

	private void initContentPager() {
		// 把4种view添加到布局中
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);

		if (loadingView == null) {
			loadingView = View.inflate(getContext(), R.layout.page_loading,null);
		}
		addView(loadingView, params);
		if (errorView == null) {
			errorView = View.inflate(getContext(), R.layout.page_error, null);
		}
		addView(errorView, params);

		if (emptyView == null) {
			emptyView = View.inflate(getContext(), R.layout.page_empty, null);
		}
		addView(emptyView, params);

		if (successView == null) {
			successView = createView();
			LogUtil.d(TAG, "successView = createView();"+successView);
		}
		if (successView != null) {
			addView(successView, params);
		} else {
			throw new IllegalArgumentException(
					"the method successView is not return NULL");
		}

		// 2根据state显示不同的view
		showPager();
		// 3加载数据并刷新UI
		loadAndRefreshData();
	}

	public void loadAndRefreshData() {
		new Thread() {
			@Override
			public void run() {
//				super.run();
				SystemClock.sleep(1500);

				Object data = loadData();
				// 根据data判断state
				PageState state = checkData(data);
				// 主线程跟新UI
				CommonUtil.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						showPager();
					}
				});
			};
		}.start();
	}

	// 判断返回的数据是集合还是javabean 
	protected PageState checkData(Object data) {
		if (data != null) {
			if (data instanceof List) {
				List list = (List) data;
				if (list.size() == 0) {
					mState = PageState.STATE_EMPTY;
				} else {
					mState = PageState.STATE_SUCCESS;
				}
			} else {
				mState = PageState.STATE_SUCCESS;
			}
		} else {
			mState = PageState.STATE_ERROR;
		}
		return mState;
	}

	private void showPager() {
		loadingView.setVisibility(View.GONE);
		successView.setVisibility(View.GONE);
		errorView.setVisibility(View.GONE);
		emptyView.setVisibility(View.GONE);

		switch (mState.getValue()) {
		case 1:
			loadingView.setVisibility(View.VISIBLE);
			break;
		case 2:
			successView.setVisibility(View.VISIBLE);
			break;
		case 3:
			errorView.setVisibility(View.VISIBLE);
			break;
		case 4:
			emptyView.setVisibility(View.VISIBLE);
			break;
		}
	}

	public PageState mState = PageState.STATE_LOADING;// 默认加载中

	// 定义界面加载的4种状态
	enum PageState {
		STATE_LOADING(1), // 加载中的状态
		STATE_SUCCESS(2), // 加载数据成功的状态
		STATE_ERROR(3), // 加载数据失败的状态
		STATE_EMPTY(4);// 加载数据为空的状态

		private int value;

		PageState(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	// 每个pager的view不同
	protected abstract View createView();

	//
	protected abstract Object loadData();

}
