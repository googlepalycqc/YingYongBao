package com.tencent.apps.ui.fragment;

import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.google.gson.reflect.TypeToken;
import com.tencent.apps.R;
import com.tencent.apps.global.AppsApplication;
import com.tencent.apps.http.HttpHelper;
import com.tencent.apps.http.Url;
import com.tencent.apps.lib.randomlayout.StellarMap;
import com.tencent.apps.lib.randomlayout.StellarMap.Adapter;
import com.tencent.apps.utils.ColorUtil;
import com.tencent.apps.utils.CommonUtil;
import com.tencent.apps.utils.GsonUtil;

import android.R.color;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract.Colors;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
/**
 * 推荐界面
 * @author cui
 * 步骤：
 *	 准备工作：导入stellarMap的4个类 
 *  1 创建adapter
 *  2 创建StellaMap对象，并设置textVeiw的内边距
 *  3 设置adapter，默认显示第0组数据，设置stellMap的XY轴规则
 */
public class RecomendFragment extends BaseFragment {
	
//    ArrayList<String> list = new ArrayList<String>();//只显示单页数据，不需要叠加，所以不需要这个list
    List<String> recomendList;//一次性拿到了所有的数据，放在此集合中
	private StellarMap stellarMap;
	@Override
	protected Object requestData() {
		String json = HttpHelper.get(Url.HOME_RECOMMEND);
		recomendList = (List<String>) GsonUtil.jsonToList(json, new TypeToken<List<String>>() {}.getType());
		//集合不为空显示数据
		if (recomendList != null) {
			CommonUtil.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					stellarMap.setAdapter(new StellarAdapter());
					//默认显示第一页
					stellarMap.setGroup(0, true);//第1组数据显示
					//设置XY轴 分成多少分
					stellarMap.setRegularity(15, 15);
				}
			});
		}
		return recomendList;
	}

	@Override
	protected View getSuccessView() {
		stellarMap = new StellarMap(context);
		//设置textview的内边距
		int padding = (int) CommonUtil.getDimension(R.dimen.stellmap_padding);
		stellarMap.setInnerPadding(padding, padding, padding, padding);
		return stellarMap;
	}

	class StellarAdapter implements Adapter{//注意是StellarMap.Adapter

		//有几组数据
		@Override
		public int getGroupCount() {
			return 3;
		}

		//每一组有多少个
		@Override
		public int getCount(int group) {
			return 11;
		}

		@Override
		public View getView(int group, int position, View convertView) {
			TextView tv = new TextView(context);
			tv.setText(recomendList.get(group*getCount(group)+position));
			
			//1 设置字体大小
			Random random = new Random();
			int textSize = random.nextInt(8)+15;
			tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize);
			
			//2 设置字体颜色
			int textColor = ColorUtil.getRandomColor();
			tv.setTextColor(textColor);
			return tv;
		}

		//没用
		@Override
		public int getNextGroupOnPan(int group, float degree) {
			return 0;
		}

		/**
		 * 下一页面显示的是第几组的数据
		 * group: 0 1 2 共3组
		 */
		@Override
		public int getNextGroupOnZoom(int group, boolean isZoomIn) {
			return (group+1)%getGroupCount();
		}
		
	}
}
