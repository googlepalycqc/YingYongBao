package com.tencent.apps.ui.fragment;

import com.tencent.apps.utils.CommonUtil;
import com.tencent.apps.utils.LogUtil;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 基类
 * 
 * @author cui
 * 
 */
public abstract class BaseFragment extends Fragment {
	private static final String TAG = "BaseFragment";
	protected ContentPager contentPager;
	protected Context context;
//   
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		context = getActivity();//不可作为成员变量
		if (contentPager == null) {
			contentPager = new ContentPager(getActivity()) {
				
				@Override
				protected Object loadData() {
					return requestData();
				}
				
				@Override
				protected View createView() {
					return getSuccessView();
				}
			};
		} else {
			CommonUtil.removeSelfFromParent(contentPager);
		}
		return contentPager;
	}

	// 每个子类的实现方式不同，所以抽象
	protected abstract Object requestData();

	protected abstract View getSuccessView();
	
	
}
