package com.tencent.apps.ui.fragment;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.tencent.apps.R;
import com.tencent.apps.bean.Category;
import com.tencent.apps.bean.Subject;
import com.tencent.apps.global.AppsApplication;
import com.tencent.apps.http.HttpHelper;
import com.tencent.apps.http.Url;
import com.tencent.apps.ui.adapter.CategoryAdapter;
import com.tencent.apps.utils.CommonUtil;
import com.tencent.apps.utils.GsonUtil;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
/**
 * 
 * @author cui
 *1
 */
public class CategoryFragment extends BaseFragment {

	private ListView listView;
	private CategoryAdapter adapter;
	
	/**
	 * 1 请求数据+解析数据
	 * 2 将数据存放在集合中
	 * 3 如果集合不为空，给listView设置适配器
	 */
	@Override
	protected Object requestData() {
		ArrayList<Object> list = new ArrayList<Object>();
		String json = HttpHelper.get(Url.HOME_CATEGORY);
		List<Category> categoryList = (List<Category>) GsonUtil.jsonToList(json, new TypeToken<List<Category>>() {}.getType()); 
		for (Category category : categoryList) {
			list.add(category.title);//String，加入的是元素
			list.addAll(category.infos);//加入的是集合中的数据，而不是集合
//			list.add(category.infos);加入的是集合，把集合作为一个元素加入到新集合中
		}
		
		if (categoryList != null && categoryList.size()>0) {
			adapter = new CategoryAdapter(list, context);
			CommonUtil.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					listView.setAdapter(adapter);
				}
			});
		}
		return categoryList;
	}

	@Override
	protected View getSuccessView() {
		listView = (ListView) View.inflate(context, R.layout.listview, null);
		
		return listView;
	}

}
