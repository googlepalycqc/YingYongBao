package com.tencent.apps.ui.fragment;

import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.util.LogUtils;
import com.tencent.apps.R;
import com.tencent.apps.global.AppsApplication;
import com.tencent.apps.http.HttpHelper;
import com.tencent.apps.http.Url;
import com.tencent.apps.ui.view.FlowLayout;
import com.tencent.apps.utils.ColorUtil;
import com.tencent.apps.utils.CommonUtil;
import com.tencent.apps.utils.DrawableUtil;
import com.tencent.apps.utils.GsonUtil;
import com.tencent.apps.utils.ToastUtil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.TextView;

public class HotFragment extends BaseFragment {

	private FlowLayout flowLayout;

	@Override
	protected Object requestData() {
		String json = HttpHelper.get(Url.HOME_HOT);
		final List<String> hotList = (List<String>) GsonUtil.jsonToList(json,
				new TypeToken<List<String>>() {
				}.getType());

		CommonUtil.runOnUiThread(new Runnable() {

			@Override
			public void run() {

				for (int i = 0; i < hotList.size(); i++) {
					final TextView textView = new TextView(context);
					//子view 中内容的padding
					int childViewHorPadding = (int) CommonUtil.getDimension(R.dimen.childview_h_padding);
					int childViewVerPadding = (int) CommonUtil.getDimension(R.dimen.childview_v_padding);
					initTextView(hotList, i, textView);
							
				
					
					
					flowLayout.addView(textView);

					textView.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							ToastUtil.showToast(textView.getText().toString());
						}
					});
				}
			}
		});
		LogUtils.d("###hotList="+hotList);
		return hotList;
	}

	public void initTextView(final List<String> hotList, int i,
			final TextView textView) {
		//子view 中内容的padding
		int childViewHorPadding = (int) CommonUtil.getDimension(R.dimen.childview_h_padding);
		int childViewVerPadding = (int) CommonUtil.getDimension(R.dimen.childview_v_padding);
				
		textView.setText(hotList.get(i));
		textView.setTextColor(Color.WHITE);
		textView.setGravity(Gravity.CENTER);
		textView.setPadding(childViewHorPadding, childViewVerPadding, childViewHorPadding, childViewVerPadding);
		textView.setTextSize( TypedValue.COMPLEX_UNIT_SP, 15);
//		textView.setBackgroundColor(ColorUtil.getRandomColor());
		
		GradientDrawable pressed = DrawableUtil.generateDrawable(ColorUtil.getRandomColor(), CommonUtil.getDimension(R.dimen.childview_radius));
		GradientDrawable normal = DrawableUtil.generateDrawable(ColorUtil.getRandomColor(), CommonUtil.getDimension(R.dimen.childview_radius));
		StateListDrawable stateListDrawable = DrawableUtil.generateSeletor(normal, pressed);
		
		textView.setBackgroundDrawable(stateListDrawable);
	}

	@Override
	protected View getSuccessView() {
		int childViewHorSpacing = (int) CommonUtil
				.getDimension(R.dimen.childview_h_spacing);
		int lineVerSpacing = (int) CommonUtil
				.getDimension(R.dimen.line_v_spacing);
		int flowLayoutPadding = (int) CommonUtil
				.getDimension(R.dimen.flowlayout_padding);

		ScrollView scrollView = new ScrollView(context);
		// scrollView.setFillViewport(true);//即使内容不够，也充满scrollViw
		flowLayout = new FlowLayout(context);
		flowLayout.childViewHorSpacing = childViewHorSpacing;
		flowLayout.lineVerSpacing = lineVerSpacing;
		flowLayout.setPadding(flowLayoutPadding, flowLayoutPadding,
				flowLayoutPadding, flowLayoutPadding);
		// flowLayout.setBackgroundColor(Color.RED);
		scrollView.addView(flowLayout);
		return scrollView;
	}
}
