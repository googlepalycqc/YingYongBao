package com.tencent.apps.ui.fragment;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.tencent.apps.R;
import com.tencent.apps.bean.AppInfo;
import com.tencent.apps.global.AppsApplication;
import com.tencent.apps.http.HttpHelper;
import com.tencent.apps.http.Url;
import com.tencent.apps.ui.adapter.AppsAdapter;
import com.tencent.apps.utils.CommonUtil;
import com.tencent.apps.utils.GsonUtil;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class GameFragment extends BaseFragment {

	ArrayList<AppInfo>  list = new ArrayList<AppInfo>();
	private PullToRefreshListView refreshListView;
	private ListView listView;
	private AppsAdapter adapter;
	
	/**
	 * list: 多次请求返回数据的集合
	 * appInfos:单次请求返回的数据
	 */
	@Override
	protected Object requestData() {
		String json = HttpHelper.get(Url.HOME_GAME+list.size());//一定要加list.size()
		List<AppInfo> appInfos = (List<AppInfo>) GsonUtil.jsonToList(json, new TypeToken<List<AppInfo>>() {}.getType());
		
		//更新UI
		if (appInfos != null) {
			list.addAll(appInfos);
			CommonUtil.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					adapter.notifyDataSetChanged();
					refreshListView.onRefreshComplete();
				}
			});
		}
		return appInfos;
	}

	@Override
	protected View getSuccessView() {
		initRefreshListView();
		
		initListView();
		
		return refreshListView;
	}

	public void initListView() {
		listView = refreshListView.getRefreshableView();
		listView.setDividerHeight(0);
		listView.setSelector(android.R.color.transparent);
		
		adapter = new AppsAdapter(list, context);
		listView.setAdapter(adapter);
	}

	public void initRefreshListView() {
		refreshListView = (PullToRefreshListView) View.inflate(context, R.layout.ptr_listview, null);
		refreshListView.setMode(Mode.BOTH);
		refreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(final PullToRefreshBase<ListView> refreshView) {
				if (refreshView.getCurrentMode() == Mode.PULL_FROM_START) {
					CommonUtil.runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							refreshView.onRefreshComplete();
						}
					});
				} else {
					contentPager.loadAndRefreshData();
				}
			}
		});
	}
}
