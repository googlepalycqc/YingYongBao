package com.tencent.apps.ui.fragment;

import com.tencent.apps.R;
import com.tencent.apps.utils.CommonUtil;

import android.support.v4.app.Fragment;

public class FragmentFactory {

	public static Fragment createFragment(int position) {

		Fragment fragment = null;//必须设置？？= new Fragment()
		switch (position) {
		case 0:
			fragment = new HomeFragment();
			break;
		case 1:
			fragment = new AppFragment();
			break;
		case 2:
			fragment = new GameFragment();
			break;
		case 3:
			fragment = new SubjectFragment();
			break;
		case 4:
			fragment = new RecomendFragment();
			break;
		case 5:
			fragment = new CategoryFragment();
			break;
		case 6:
			fragment = new HotFragment();
			break;
		default:
			break;
		}
		return fragment;
	}
	
	
	// 返回 页面的数量CommonUtil.getStringArray(R.array.tab_names)。length（）
	public static int getFragmentNum(){
		return 7;
	} 
}
