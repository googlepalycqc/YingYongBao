package com.tencent.apps.ui.adapter;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.tencent.apps.R;
import com.tencent.apps.bean.Subject;
import com.tencent.apps.global.ImageLoaderOptions;
import com.tencent.apps.http.Url;
import com.tencent.apps.ui.view.RatioImageView;
import com.tencent.apps.utils.LogUtil;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class SubjectAdapter extends BasicAdapter<Subject> {

	public SubjectAdapter(ArrayList<Subject> list, Context context) {
		super(list, context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.adapter_subject, null);
		}
		SubjectViewHolder holder = SubjectViewHolder.getSubjectViewHolder(convertView);

		Subject sub = list.get(position);
		holder.tv_des.setText(sub.des);
		//   Url.HOME + sub.url
		ImageLoader.getInstance().displayImage(Url.HOME_IMAGE + sub.url,
				holder.iv_image, ImageLoaderOptions.pagerOptions);//RoundedBitmapDisplayer 当imageview的height= wrap_content高可能显示不出来。
		return convertView;
	}

	public static class SubjectViewHolder {

		public RatioImageView iv_image;
		public TextView tv_des;

		public SubjectViewHolder(View convertView) {
			super();
			iv_image = (RatioImageView) convertView.findViewById(R.id.iv_image);
			tv_des = (TextView) convertView.findViewById(R.id.tv_des);
		}

		public static SubjectViewHolder getSubjectViewHolder(View convertView) {
			SubjectViewHolder holder = (SubjectViewHolder) convertView.getTag();
			if (holder == null) {
				holder = new SubjectViewHolder(convertView);
				convertView.setTag(holder);
			}
			return holder;
		}
	}
}
