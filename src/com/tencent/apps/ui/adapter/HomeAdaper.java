package com.tencent.apps.ui.adapter;

import java.util.ArrayList;

import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tencent.apps.R;
import com.tencent.apps.bean.AppInfo;
import com.tencent.apps.global.ImageLoaderOptions;
import com.tencent.apps.http.Url;
import com.tencent.apps.utils.LogUtil;

import android.content.Context;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class  HomeAdaper<T> extends BasicAdapter<T>{

	

	/*
	 * 构造方法是不可以被继承的，所以这里要手写，然后super（。。）调用父类的构造方法。
	 */
	public HomeAdaper(ArrayList<T> list, Context context) {
		super(list, context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//旧写法：
		ViewHolder holder;
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.adapter_home, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		//新方法：
//		if (convertView == null) {
//			convertView = LayoutInflater.from(context).inflate(R.layout.adapter_home, null);
//		}
//		ViewHolder holder = ViewHolder.getHolder(convertView);
		
		AppInfo appInfo = (AppInfo) list.get(position);
		holder.tv_name.setText(appInfo.name);
		holder.tv_des.setText(appInfo.des);
		holder.tv_size.setText(Formatter.formatFileSize(context,appInfo.size));
		holder.rb_stars.setRating(appInfo.stars);
		ImageLoader.getInstance().displayImage(Url.HOME_IMAGE+appInfo.iconUrl, holder.iv_icon, ImageLoaderOptions.options);
		
		/**
		 * 给item添加缩放特效动画
		 * 步骤： 1 先缩放到一半0.5
		 * 		2 在还原
		 */
		ViewHelper.setScaleX(convertView, 0.5f);
		ViewHelper.setScaleY(convertView, 0.5f);
		ViewPropertyAnimator.animate(convertView)
							.scaleX(1f)
							.scaleY(1f)
							.setDuration(1000)
							.setInterpolator(new OvershootInterpolator(1f))//弹性插值器，参数是弹性得偏移量，从int倍大小到本身大小
							.start();
		
		return convertView;
	}

	public static class ViewHolder{

		public ImageView iv_icon;
		public TextView tv_name;
		public RatingBar rb_stars;
		public TextView tv_size;
		public TextView tv_des;

		public ViewHolder(View convertView) {
			super();
			iv_icon = (ImageView) convertView.findViewById(R.id.iv_icon);
			tv_name = (TextView) convertView.findViewById(R.id.tv_name);
			rb_stars = (RatingBar) convertView.findViewById(R.id.rb_stars);
			tv_size = (TextView) convertView.findViewById(R.id.tv_size);
			tv_des = (TextView) convertView.findViewById(R.id.tv_des);
		}
		public static ViewHolder getHolder(View convertView){
			ViewHolder holder = (ViewHolder) convertView.getTag();
			if (holder ==  null) {
				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			}
			return holder;
		}
	}
}
