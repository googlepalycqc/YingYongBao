package com.tencent.apps.ui.adapter;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tencent.apps.global.ImageLoaderOptions;
import com.tencent.apps.http.Url;
import com.tencent.apps.lib.photoview.PhotoViewAttacher;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ImageScaleAdapter extends PagerAdapter {

	public Context context;
	public ArrayList<String> list;
	public int currentNum;
	public ImageScaleAdapter(Context context,ArrayList<String> list,int currentNum) {
		super();
		this.context = context;
		this.list = list;
		this.currentNum = currentNum;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView imageView = new ImageView(context);
		final PhotoViewAttacher attacher = new PhotoViewAttacher(imageView);
		ImageLoader.getInstance().displayImage(Url.HOME_IMAGE + list.get(position), imageView, 
				ImageLoaderOptions.pagerOptions,new ImageLoadingListener() {
					
					@Override
					public void onLoadingStarted(String imageUri, View view) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
						attacher.update();//更新imageview的宽高
					}
					
					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						// TODO Auto-generated method stub
						
					}
				});
		container.addView(imageView);
		return imageView;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View)object);
	}
}
