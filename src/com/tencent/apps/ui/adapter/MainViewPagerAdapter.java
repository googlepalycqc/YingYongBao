package com.tencent.apps.ui.adapter;

import com.tencent.apps.R;
import com.tencent.apps.global.AppsApplication;
import com.tencent.apps.ui.fragment.FragmentFactory;
import com.tencent.apps.utils.CommonUtil;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainViewPagerAdapter extends FragmentPagerAdapter {//viewpager里面放的是fragment，所以adapter用FragmentPagerAdapter

	private String[] titleArrays;

	public MainViewPagerAdapter(FragmentManager fm) {
		super(fm);
		titleArrays = CommonUtil.getStringArray(R.array.tab_names);
	}

	@Override
	public Fragment getItem(int position) {
		//根据position创建fragment
		return FragmentFactory.createFragment(position);
	}

	@Override
	public int getCount() {
		return titleArrays.length;
	}

//	public MainViewPagerAdapter() {
//		super();
//	}
//	@Override
//	public int getCount() {
//		return CommonUtil.getStringArray(R.array.tab_names).length;
//	}
//
//	@Override
//	public boolean isViewFromObject(View view, Object obj) {
//		return view == obj;
//	}
//
//	@Override
//	public Object instantiateItem(ViewGroup container, int position) {
//		TextView textView = new TextView(AppsApplication.getContext());
//		textView.setText("yingyongbao");
//		container.addView(textView);
//		return textView;
//	}
//	
//	@Override
//	public void destroyItem(ViewGroup container, int position, Object object) {
//		container.removeView((View)object);
//	}
//	
	@Override
	public CharSequence getPageTitle(int position) {
		
		return titleArrays[position];
	}
}
