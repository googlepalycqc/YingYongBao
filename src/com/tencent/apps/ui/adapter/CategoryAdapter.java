package com.tencent.apps.ui.adapter;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.tencent.apps.R;
import com.tencent.apps.bean.CategoryInfo;
import com.tencent.apps.global.ImageLoaderOptions;
import com.tencent.apps.http.Url;
import com.tencent.apps.utils.ToastUtil;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CategoryAdapter extends BasicAdapter<Object> {
	
	//定义2中类型的值
	public final int ITEM_TITLE =0;
	public final int ITEM_INFO =1;
	
	public CategoryAdapter(ArrayList<Object> list, Context context) {
		super(list, context);
	}
	
	//返回item类型的  数量
	@Override
	public int getViewTypeCount() {
		return 2;
	}
	//返回item类型, 若有2个类型，那么ItemViewType是0和1。(从0开始)
	@Override
	public int getItemViewType(int position) {
		if (list.get(position) instanceof String) {
			return ITEM_TITLE;
		} else {
			return ITEM_INFO;
		}
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		int itemViewType = getItemViewType(position);
		switch (itemViewType) {
		case ITEM_TITLE:
			if (convertView == null) {
				convertView = View.inflate(context, R.layout.adapter_category_title, null);
			}
			TextView tv_title = (TextView) convertView.findViewById(R.id.tv_title);
			tv_title.setText((String)list.get(position));
			break;
		case ITEM_INFO:
			CategoryHolder holder ;
			if (convertView == null) {
				convertView = View.inflate(context, R.layout.adapter_category_info, null);
				holder = new CategoryHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (CategoryHolder) convertView.getTag();
			}
			
			final CategoryInfo info = (CategoryInfo) list.get(position);
			
			//无论哪一行，第一个肯定存在
			holder.tv_name1.setText(info.name1);
			ImageLoader.getInstance().displayImage(Url.HOME_IMAGE+info.url1, holder.iv_image1, ImageLoaderOptions.options);
			holder.layout_info1.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ToastUtil.showToast(info.name1);
				}
			});
			
			//判断第2个是否存在
			if (TextUtils.isEmpty(info.name2)) {
				holder.layout_info2.setVisibility(View.INVISIBLE);
			} else {
				holder.layout_info2.setVisibility(View.VISIBLE);
				holder.tv_name2.setText(info.name2);
				ImageLoader.getInstance().displayImage(Url.HOME_IMAGE+info.url2, holder.iv_image2, ImageLoaderOptions.options);
				holder.layout_info2.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						ToastUtil.showToast(info.name2);
					}
				});
			}
			
			//判断第3个是否存在
			if (TextUtils.isEmpty(info.name3)) {
				holder.layout_info3.setVisibility(View.INVISIBLE);
			} else {
				holder.layout_info3.setVisibility(View.VISIBLE);
				holder.tv_name3.setText(info.name3);
				ImageLoader.getInstance().displayImage(Url.HOME_IMAGE+info.url3, holder.iv_image3, ImageLoaderOptions.options);
				holder.layout_info3.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						ToastUtil.showToast(info.name3);
					}
				});
			}
			break;
		default:
			break;
		}
		return convertView;
	}

	class CategoryHolder {

		public ImageView iv_image1;
		public ImageView iv_image2;
		public ImageView iv_image3;
		
		public TextView tv_name1;
		public TextView tv_name2;
		public TextView tv_name3;
		
		public LinearLayout layout_info1;
		public LinearLayout layout_info2;
		public LinearLayout layout_info3;

		public CategoryHolder(View convertView) {
			super();
			iv_image1 = (ImageView) convertView.findViewById(R.id.iv_image1);
			iv_image2 = (ImageView) convertView.findViewById(R.id.iv_image2);
			iv_image3 = (ImageView) convertView.findViewById(R.id.iv_image3);
			
			tv_name1 = (TextView) convertView.findViewById(R.id.tv_name1);
			tv_name2 = (TextView) convertView.findViewById(R.id.tv_name2);
			tv_name3 = (TextView) convertView.findViewById(R.id.tv_name3);
			
			layout_info1 = (LinearLayout) convertView.findViewById(R.id.ll_info1);
			layout_info2 = (LinearLayout) convertView.findViewById(R.id.ll_info2);
			layout_info3 = (LinearLayout) convertView.findViewById(R.id.ll_info3);
		}
	}
}
