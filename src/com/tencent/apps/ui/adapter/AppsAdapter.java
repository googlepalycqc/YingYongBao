package com.tencent.apps.ui.adapter;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.tencent.apps.R;
import com.tencent.apps.bean.AppInfo;
import com.tencent.apps.global.ImageLoaderOptions;
import com.tencent.apps.http.Url;

import android.content.Context;
import android.text.format.Formatter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class AppsAdapter extends BasicAdapter<AppInfo> {

	public AppsAdapter(ArrayList<AppInfo> list, Context context) {
		super(list, context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		AppViewHolder holder ;
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.adapter_home, null);
			holder = new AppViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (AppViewHolder) convertView.getTag();
		}
		
		AppInfo info = list.get(position);
		holder.tv_name.setText(info.name);
		holder.tv_size.setText(Formatter.formatFileSize(context, info.size));//格式化
		holder.tv_des.setText(info.des);
		holder.rb_stars.setRating(info.stars);
		
		ImageLoader.getInstance().displayImage(Url.HOME_IMAGE+info.iconUrl, holder.iv_icon, ImageLoaderOptions.options);
		
		return convertView;
	}
	
	public class AppViewHolder {
		public ImageView iv_icon;
		public TextView tv_name;
		public TextView tv_des;
		public TextView tv_size;
		public RatingBar rb_stars;

		public AppViewHolder(View convertView){
			iv_icon = (ImageView) convertView.findViewById(R.id.iv_icon);
			tv_name = (TextView) convertView.findViewById(R.id.tv_name);
			tv_des = (TextView) convertView.findViewById(R.id.tv_des);
			tv_size = (TextView) convertView.findViewById(R.id.tv_size);
			rb_stars = (RatingBar) convertView.findViewById(R.id.rb_stars);
		}
	}
}
