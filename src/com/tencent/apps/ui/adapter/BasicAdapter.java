package com.tencent.apps.ui.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class BasicAdapter<T> extends BaseAdapter {

	// public BasicAdapter(ArrayList<T> list) {
	// super();
	// this.list = list;
	// }
	public ArrayList<T> list;
	public Context context;

	public BasicAdapter(ArrayList<T> list, Context context) {
		super();
		this.list = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	// 返回null，在让子类重写这个方法
	// @Override
	// public View getView(int position, View convertView, ViewGroup parent) {
	// // TODO Auto-generated method stub
	// return null;
	// }

	// D:如果一个类是抽象类，那么，继承它的子类
	// 要么是抽象类。 cui:而抽象类是不需要重写父类所有的抽象方法的。 所以此处继承了BaseAdapter，但是可以不重写getView（）
	// 要么重写所有抽象方法。
	public abstract View getView(int position, View convertView,
			ViewGroup parent);
}
