package com.tencent.apps.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.tencent.apps.global.ImageLoaderOptions;
import com.tencent.apps.http.Url;
import com.tencent.apps.utils.LogUtil;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class HomePagerAdapter extends PagerAdapter {

	public ArrayList<String> urlList;
	public Context context;

	public HomePagerAdapter(ArrayList<String> urlList,Context context) {
		super();
		this.urlList = urlList;
		this.context = context;
	}

	@Override
	public int getCount() {
//		return urlList.size();
		return  Integer.MAX_VALUE;//Integer.MAX_VALUE 实现viewpaper的无线循环
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView imageView = new ImageView(context);
														//注意是%，不是/;position/urlList.size()实现viewpaper的无线循环
		ImageLoader.getInstance().displayImage(Url.HOME_IMAGE+urlList.get(position%urlList.size()), imageView, ImageLoaderOptions.pagerOptions);
		container.addView(imageView);
		return imageView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}
}
