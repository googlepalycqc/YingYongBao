package com.tencent.apps.ui.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FlowLayout extends ViewGroup {

	public int childViewHorSpacing = 12;//子view的水平间距
	public int lineVerSpacing = 12;//行与行的  的间距
	public List<Line> lineList = new ArrayList<FlowLayout.Line>();


	public FlowLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public FlowLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * 
	 * @param context
	 */
	public FlowLayout(Context context) {
		super(context);
	}
	
	/**
	 * 测量width height
	 * 实现line的分行操作。
	 * height=layoutPaddingTop + layoutPaddingBottom + LineHeight + childViewVerSpacing
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);//放在后面可以吗？
		//1 得到width
		int width = MeasureSpec.getSize(widthMeasureSpec);
		//2 得到实际的宽
		int noPadingWidth = width - getPaddingLeft() - getPaddingRight();//?
		
		
		//3 实现分行逻辑
		Line line = null;
		for (int i = 0; i < getChildCount(); i++) {
			View childView = getChildAt(i);
			childView.measure(0, 0);// 引起childView的onMeasure方法回调，如果传入0，则会按照控件本身的宽高参数去测量
			
			//初始化line对象，若换行就不是同一个line对象了
			if (line == null) {
				line= new Line();
			}
			
			//4 往line中添加字view
			if (line.childViewList.size() == 0) {//集合中没有子view，直接添加即可
				line.addChildView(childView);
			} else {//有子view，需要判断宽度是否超标
				if (line.LineWidth + childView.getMeasuredWidth() + childViewHorSpacing > noPadingWidth) {
					//先保存之前的line，再创建新的line对象（换行）
					lineList.add(line);
					line = new Line();
					line.addChildView(childView);
				} else{
					line.addChildView(childView);
				}
			}
			
			//如果是最后一个，保存line对象
			if (i == (getChildCount() - 1)) {
				lineList.add(line);
			}
		}
		
		
		//求FlowLayout的高
		int height = initHeight();
		
		setMeasuredDimension(width, height);// 设置flowLayout的宽高
	}

	public int initHeight() {
		int height1 = getPaddingTop() + getPaddingBottom();
		int height2 = 0 ;
		for (int i = 0; i < lineList.size(); i++) {
			height2 = height2+ lineList.get(i).LineHeight;
		}
		int height3 = lineVerSpacing * (lineList.size() - 1);
		int height = height1 + height2 + height3;
		return height;
	}
	
	/**
	 * 摆放子view在FlowLayout中的位置
	 * 1 遍历LineList,得到每一个line，
	 * 2 遍历line，得到childViewList
	 * 3 计算留白区域，平均分给子view
	 * 4 摆放子view
	 */
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		int paddingLeft = getPaddingLeft();
		int paddingTop = getPaddingTop();
		//1 遍历LineList,得到每一个line，
		for (int i = 0; i < lineList.size(); i++) {
			Line line = lineList.get(i);
			
			//换行要改变padingTop，下一行比上一行 多上一行的高 + 竖直间隔
			if (i > 0) {
				paddingTop = paddingTop + lineVerSpacing + lineList.get(i -1).LineHeight;
			}
			
			//计算留白区域(若当前行的宽不到右边，会有空白)
			int remainSpacing = getMeasuredWidth() - line.LineWidth - getPaddingLeft() - getPaddingRight();
			int perSpacing = remainSpacing/(line.childViewList.size());
			
			// 2 遍历line，得到childViewList
			for (int j = 0; j < line.childViewList.size(); j++) {
				View childView = line.childViewList.get(j);
				
				//将平均留白 加到每一个 childView的宽中
				int widthMeasureSpec = MeasureSpec.makeMeasureSpec(childView.getMeasuredWidth() + perSpacing, MeasureSpec.EXACTLY);
				childView.measure(widthMeasureSpec , 0);
				
				//摆放childView，判断是不是第一个
				if (j == 0) {
					childView.layout(paddingLeft, paddingTop, paddingLeft + childView.getMeasuredWidth(), paddingTop+childView.getMeasuredHeight());
				} else {//根据前一个view来摆放自己
					View preChildView = line.childViewList.get(j-1);
					int paddingLeft2 = preChildView.getRight() + childViewHorSpacing;
					int paddingTop2 = preChildView.getTop();
					int paddingRight2 = paddingLeft2 + childView.getMeasuredWidth();
					int paddingBottom2 = preChildView.getBottom();
					
					childView.layout(paddingLeft2, paddingTop2, paddingRight2, paddingBottom2);
				}
			}
		}
		
	}
	
	
	/**
	 * Line是用来存放ChildView
	 * @author cui
	 * LineWidth   LineHeight  childViewList
	 */
	class Line{
		public int LineWidth;
		public int LineHeight=0;//int类型可以不进行初始化，因为会有默认初始化为0
		public List<View> childViewList = new ArrayList<View>();//??
		
		public Line() {
			super();
		}
		/**
		 * 往childViewList中添加ChildView
		 * @param ChildView
		 *  当childViewList 中没有ChildView才添加
		 *  
		 */
		public void addChildView(View childView){
			if (!childViewList.contains(childView)) {
				if (childViewList.size() == 0) {
					LineWidth = childView.getMeasuredWidth();//??getWidth()
				} else {
					LineWidth = LineWidth + childView.getMeasuredWidth()+childViewHorSpacing;
				}
				LineHeight = Math.max(LineHeight, childView.getMeasuredHeight());
				
				childViewList.add(childView);
			}
		}
		
	}
}
