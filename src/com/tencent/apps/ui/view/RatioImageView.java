package com.tencent.apps.ui.view;

import com.tencent.apps.R.attr;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
/**
 * 根据指定的宽高比，动态设置imageview的高
 * @author cui
 *
 */
public class RatioImageView extends ImageView {
	public float ratio = 0f;
	public RatioImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/**
	 * xml 中使用自定义属性的时候，走这个方法
	 * @param context
	 * @param attrs
	 */
	public RatioImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		ratio = attrs.getAttributeFloatValue("http://schemas.android.com/apk/res/com.tencent.apps", "ratio", 0f);
	}

	public RatioImageView(Context context) {
		super(context);
	}
	/**
	 * 要复用这个类ratio就必须动态设置
	 * 	方法一：setRatio(int ratio)
	 *  方法二：自定义属性attrx.xml
	 */
	public  void setRatio(int ratio){
		this.ratio = ratio;
	}
	
	/**
	 * MeasureSpec: 测量规则，由size和mode组成
	 * size : 表示当前控件具体的像素值
	 * mode ： 测量的模式，共三种：
	 *      MeasureSpec.AT_MOST ： 对应的是wrap_content;
	 *      MeasureSpec.EXACTLY : 对应的是match_parent,具体的dp值
	 *      MeasureSpec.UNSPECIFIED : 未定义的，一般多用于ListView对adapter的View测量中
	 *      
	 * widthMeasureSpec: 是父View通过size和mode计算好的测量规则
	 * heightMeasureSpec: 是父View通过size和mode计算好的测量规则
	 */
	/**
	 * 1 根据测量规则拿到宽
	 * 2 根据宽高比+宽  得到高
	 * 3 将高做成测量规则中
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		if (ratio != 0) {
			int height = (int) (width/ratio);
			heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
		}
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
}
