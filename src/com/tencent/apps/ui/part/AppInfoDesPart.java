package com.tencent.apps.ui.part;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.tencent.apps.R;
import com.tencent.apps.bean.AppInfo;
import com.tencent.apps.global.AppsApplication;
import com.tencent.apps.utils.LogUtil;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * 
 * @author cui 1 创建view
 */
public class AppInfoDesPart extends BasePart<AppInfo> {

	public Context context = AppsApplication.getContext();
	private TextView tv_des;
	private TextView tv_author;
	private ImageView iv_des_arrow;
	private ScrollView scrollView;

	//
	@Override
	public View getView() {
		View view = View.inflate(context, R.layout.layout_detail_app_des, null);
		tv_des = (TextView) view.findViewById(R.id.tv_des);
		tv_author = (TextView) view.findViewById(R.id.tv_author);
		iv_des_arrow = (ImageView) view.findViewById(R.id.iv_des_arrow);

		return view;
	}

	public AppInfoDesPart(ScrollView scrollView) {
		super();
		this.scrollView = scrollView;
	}

	private int minHeight;
	private int maxHeight;
	boolean isOpen = false;

	/**
	 * 主要是 1 拿到5行的高度 2 拿到全部内容时的高度
	 */
	@Override
	public void setView(AppInfo info) {
		tv_des.setText(info.des);
		tv_author.setText(info.author);

		tv_des.setMaxLines(5);
		tv_des.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

					// 全局layout()方法后执行这个方法
					@Override
					public void onGlobalLayout() {
						// 取出监听
						tv_des.getViewTreeObserver().removeGlobalOnLayoutListener(this);
						minHeight = tv_des.getMeasuredHeight();

						tv_des.setMaxLines(Integer.MAX_VALUE);
						tv_des.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

									// 全局layout()方法后执行这个方法
									@Override
									public void onGlobalLayout() {
										// 取出监听
										tv_des.getViewTreeObserver().removeGlobalOnLayoutListener(this);
										maxHeight = tv_des.getMeasuredHeight();

										// 默认显示5行
										tv_des.getLayoutParams().height = minHeight;
										tv_des.requestLayout();
									}
								});
					}
				});

		//方法一：直接在if else中做了动画
//		iv_des_arrow.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// 没有动画
//				// if (isOpen) {
//				// tv_des.getLayoutParams().height = minHeight;
//				// tv_des.requestLayout();
//				// isOpen = false;
//				// } else {
//				// tv_des.getLayoutParams().height = maxHeight;
//				// tv_des.requestLayout();
//				// scrollView.scrollBy(0, 1000);//伸展时，scrollview向下滑动
//				// isOpen = true;
//				// }
//				ValueAnimator animator = null;
//				if (isOpen) {
//					ViewPropertyAnimator.animate(iv_des_arrow).rotationBy(180).setDuration(500).start();
//					animator = ValueAnimator.ofInt(maxHeight, minHeight);
//					animator.addUpdateListener(new AnimatorUpdateListener() {
//
//						@Override
//						public void onAnimationUpdate(
//								ValueAnimator valueAnimator) {
//							int animatedValue = (Integer) valueAnimator
//									.getAnimatedValue();
//							tv_des.getLayoutParams().height = animatedValue;
//							tv_des.requestLayout();
//						}
//					});
//					isOpen = false;
//				} else {
//					ViewPropertyAnimator.animate(iv_des_arrow).rotationBy(180).setDuration(350).start();
//					animator = ValueAnimator.ofInt(minHeight, maxHeight);
//					animator.addUpdateListener(new AnimatorUpdateListener() {
//
//						@Override
//						public void onAnimationUpdate(
//								ValueAnimator valueAnimator) {
//							int animatedValue = (Integer) valueAnimator
//									.getAnimatedValue();
//							tv_des.getLayoutParams().height = animatedValue;
//							tv_des.requestLayout();
//							scrollView.scrollBy(0, 10000);// 伸展时，scrollview向下滑动
//						}
//					});
//					isOpen = true;
//				}
//
//				animator.setDuration(500);
//				animator.start();
//			}
//		});
		
		iv_des_arrow.setOnClickListener(new OnClickListener() {
			
			private ValueAnimator animator;

			@Override
			public void onClick(View v) {
				if (isOpen) {
					animator = ValueAnimator.ofInt(maxHeight,minHeight);
				} else {
					animator = ValueAnimator.ofInt(minHeight,maxHeight);
				}
				
				animator.addUpdateListener(new AnimatorUpdateListener() {
					
					@Override
					public void onAnimationUpdate(ValueAnimator valueAnimator) {
						int height =  (Integer) valueAnimator.getAnimatedValue();
						tv_des.getLayoutParams().height = height;
						tv_des.requestLayout();
						
						if (!isOpen) {
							scrollView.scrollBy(0, 1000);
						}
					}
				});
				animator.setDuration(500).addListener(new DesAnimatorListener());
				animator.start();
			}
		});
	}

	class DesAnimatorListener implements AnimatorListener {

		@Override
		public void onAnimationCancel(Animator arg0) {
			
		}

		@Override
		public void onAnimationEnd(Animator arg0) {
			isOpen = !isOpen;
		}

		@Override
		public void onAnimationRepeat(Animator arg0) {
			
		}

		@Override
		public void onAnimationStart(Animator arg0) {
			ViewPropertyAnimator.animate(iv_des_arrow).rotationBy(180).start();
		}
	}
}
