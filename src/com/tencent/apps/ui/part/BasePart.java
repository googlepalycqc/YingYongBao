package com.tencent.apps.ui.part;

import com.tencent.apps.bean.AppInfo;

import android.content.Context;
import android.view.View;
/**
 * 所有模块的基类
 * @author cui
 *
 * @param <T>
 */
public abstract class BasePart<T> {
	/**
	 * 获取当前模块的View
	 * @return
	 */
	public abstract View getView();
	
	/**
	 * 赋值操作及其他逻辑
	 * @param t
	 */
	public abstract void setView(T t);
}
