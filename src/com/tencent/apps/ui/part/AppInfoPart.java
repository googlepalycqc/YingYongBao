package com.tencent.apps.ui.part;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.tencent.apps.R;
import com.tencent.apps.bean.AppInfo;
import com.tencent.apps.global.AppsApplication;
import com.tencent.apps.global.ImageLoaderOptions;
import com.tencent.apps.http.Url;

import android.content.Context;
import android.text.format.Formatter;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class AppInfoPart extends BasePart<AppInfo> {

	public Context context = AppsApplication.getContext();
	private ImageView iv_icon;
	private TextView tv_name;
	private RatingBar rb_stars;
	private TextView tv_download_num;
	private TextView tv_version;
	private TextView tv_date;
	private TextView tv_size;
	@Override
	public void setView(AppInfo info) {
		ImageLoader.getInstance().displayImage(Url.HOME_IMAGE + info.iconUrl, iv_icon, ImageLoaderOptions.pagerOptions);
		tv_name.setText(info.name);
		tv_download_num.setText("下载"+info.downloadNum);
		tv_version.setText("版本"+info.version);
		tv_date.setText("日期"+info.date);
		tv_size.setText("大小"+Formatter.formatFileSize(context, info.size));
		rb_stars.setRating(info.stars);

	}

	@Override
	public View getView() {
		View view = View.inflate(context, R.layout.layout_detail_app_info, null);
		iv_icon = (ImageView) view.findViewById(R.id.iv_icon);
		tv_name = (TextView) view.findViewById(R.id.tv_name);
		rb_stars = (RatingBar) view.findViewById(R.id.rb_stars);
		tv_download_num = (TextView) view.findViewById(R.id.tv_download_num);
		tv_version = (TextView) view.findViewById(R.id.tv_version);
		tv_date = (TextView) view.findViewById(R.id.tv_date);
		tv_size = (TextView) view.findViewById(R.id.tv_size);
		
		return view;
	}

}
