package com.tencent.apps.ui.part;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.tencent.apps.R;
import com.tencent.apps.bean.AppInfo;
import com.tencent.apps.global.ImageLoaderOptions;
import com.tencent.apps.http.Url;
import com.tencent.apps.ui.activity.ImageScaleActivity;
import com.tencent.apps.utils.CommonUtil;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class ImageScalePart extends BasePart<AppInfo> {

	
	public  Context context;
	private LinearLayout layout_Image;
	public ImageScalePart(Context context) {
		super();
		this.context = context;
	}

	@Override
	public View getView() {
		View view = View.inflate(context, R.layout.layout_detail_app_screen, null);
		layout_Image = (LinearLayout) view.findViewById(R.id.ll_screen);
		return view;
	}

	/**
	 * for循环中创建3个imageView ,添加到layout中
	 */
	@Override
	public void setView(AppInfo info) {
		int width = (int) CommonUtil.getDimension(R.dimen.app_image_width);
		int height = (int) CommonUtil.getDimension(R.dimen.app_image_height);
		int margin = (int) CommonUtil.getDimension(R.dimen.app_image_margin);
		final ArrayList<String> screenList = info.screen;
		for (int i = 0; i < screenList.size(); i++) {
			ImageView imageView = new ImageView(context);
			LayoutParams params = new LayoutParams(width, height);//直接写是px
			if (i>0) {//从第2张图片开始，才有的margin
				params.leftMargin = margin;
			}
			imageView.setLayoutParams(params);
			ImageLoader.getInstance().displayImage(Url.HOME_IMAGE + screenList.get(i), imageView, ImageLoaderOptions.pagerOptions);
			
			layout_Image.addView(imageView);
			
			//点击事件
			final int temp = i;//i不能用final修饰
			imageView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, ImageScaleActivity.class);
					intent.putStringArrayListExtra("screenList", screenList);
					intent.putExtra("currentNum", temp);
					context.startActivity(intent);
				}
			});
		}
		

	}

}
