package com.tencent.apps.ui.part;

import java.util.ArrayList;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tencent.apps.R;
import com.tencent.apps.bean.AppInfo;
import com.tencent.apps.bean.SafeInfo;
import com.tencent.apps.global.AppsApplication;
import com.tencent.apps.global.ImageLoaderOptions;
import com.tencent.apps.http.Url;

import android.R.anim;
import android.content.Context;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
/**
 * appitem安全模块
 * @author cui
 * 
 */
public class AppInfoSafePart extends BasePart<AppInfo> {
	public Context context = AppsApplication.getContext();
	private ImageView iv_image1;
	private ImageView iv_image2;
	private ImageView iv_image3;
	private ImageView iv_safe_arrow;
	private ImageView iv_des_image1;
	private ImageView iv_des_image2;
	private ImageView iv_des_image3;
	private TextView tv_safe_des1;
	private TextView tv_safe_des2;
	private TextView tv_safe_des3;
	private RelativeLayout rl_app_safe;
	private LinearLayout ll_des_container;
	private LinearLayout ll_des2;
	private LinearLayout ll_des3;
	 boolean isOpen = false;
	private int height;
	@Override
	public View getView() {
		View view = View.inflate(context, R.layout.layout_detail_app_safe, null);
		
		iv_image1 = (ImageView) view.findViewById(R.id.iv_image1);
		iv_image2 = (ImageView) view.findViewById(R.id.iv_image2);
		iv_image3 = (ImageView) view.findViewById(R.id.iv_image3);
		iv_safe_arrow = (ImageView) view.findViewById(R.id.iv_safe_arrow);
		iv_des_image1 = (ImageView) view.findViewById(R.id.iv_des_image1);
		iv_des_image2 = (ImageView) view.findViewById(R.id.iv_des_image2);
		iv_des_image3 = (ImageView) view.findViewById(R.id.iv_des_image3);
		tv_safe_des1 = (TextView) view.findViewById(R.id.tv_safe_des1);
		tv_safe_des2 = (TextView) view.findViewById(R.id.tv_safe_des2);
		tv_safe_des3 = (TextView) view.findViewById(R.id.tv_safe_des3);
		
		rl_app_safe = (RelativeLayout) view.findViewById(R.id.rl_app_safe);
		ll_des_container = (LinearLayout) view.findViewById(R.id.ll_des_container);
		ll_des2 = (LinearLayout) view.findViewById(R.id.ll_des2);
		ll_des3 = (LinearLayout) view.findViewById(R.id.ll_des3);
		
		/**
		 * 给rl_app_safe添加动画效果
		 * 1 先移出屏幕屏幕的宽
		 * 2 在移回来
		 */
	/*	WindowManager  winManager = (WindowManager) context.getSystemService(context.WINDOW_SERVICE);
		int width = winManager.getDefaultDisplay().getWidth();
		int height = winManager.getDefaultDisplay().getHeight();
		ViewHelper.setTranslationX(rl_app_safe, -width);
		ViewPropertyAnimator.animate(rl_app_safe)
							.translationXBy(width)
							.setDuration(1000)
							.setInterpolator(new OvershootInterpolator(4))
//							.setInterpolator(new BounceInterpolator())
//							.setInterpolator(new CycleInterpolator(4))最后消失了,为什么？
							.setStartDelay(2000)
							.start();*/
		
//		rl_app_safe.setOnClickListener(new OnClickListener() {
//			
//			public void onClick(View v) {
//				if (isOpen ==true) {
////					ll_des_container.setVisibility(View.GONE);
//					ll_des_container.getLayoutParams().height = 0;
//					ll_des_container.requestLayout();
//					isOpen = false;
//				} else{
////					ll_des_container.setVisibility(View.VISIBLE);
//					ll_des_container.getLayoutParams().height = height;
//					ll_des_container.requestLayout();
//					isOpen = true;
//				}
//			}
//		});
		
//		//实现 隐藏与显示的效果方法二：方法一android:visibility="gone"
//		//测量得到高
//		ll_des_container.measure(0, 0);
//		height = ll_des_container.getMeasuredHeight();
//		//方法一：将高设置为0，达到隐藏的效果
//		ll_des_container.getLayoutParams().height = 0;
//		ll_des_container.requestLayout();
		
		//方法三：
		ll_des_container.measure(0, 0);
		height = ll_des_container.getMeasuredHeight();
		
		rl_app_safe.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				ValueAnimator animator = null;
				if (isOpen) {
					animator = ValueAnimator.ofInt(height,0);
				} else {
					animator = ValueAnimator.ofInt(0,height);
				}
				
				animator.addUpdateListener(new AnimatorUpdateListener() {
					
					@Override
					public void onAnimationUpdate(ValueAnimator valueAnimator) {
						int value = (Integer) valueAnimator.getAnimatedValue();
						ll_des_container.getLayoutParams().height = value;
						ll_des_container.requestLayout();
					}
				});
				animator.setDuration(500)
						.addListener(new SafeAnimatorListener());
				animator.start();
			}
		});		
		
		//默认不打开
		ll_des_container.getLayoutParams().height = 0;
		ll_des_container.requestLayout();
		return view;
	}
	//动画监听
	class SafeAnimatorListener implements AnimatorListener{

		@Override
		public void onAnimationCancel(Animator animator) {
			
		}

		@Override
		public void onAnimationEnd(Animator animator) {
			isOpen = !isOpen;
		}

		@Override
		public void onAnimationRepeat(Animator animator) {
			
		}

		@Override
		public void onAnimationStart(Animator animator) {
			ViewPropertyAnimator.animate(iv_safe_arrow).rotationBy(180).setDuration(350).start();
			//练习属性动画
/*			ViewPropertyAnimator.animate(iv_image1)//作用于哪个控件
				.translationXBy(50)
//				.rotationBy(360)//旋转度数
				.setDuration(3000)//持续时长
				.setInterpolator(new OvershootInterpolator(5))//弹性插值器，幅度均匀
//				.setInterpolator(new CycleInterpolator(3))//重复3次
//				.setInterpolator(new BounceInterpolator())//球落地插值器，幅度越来越小
				.start();*/
		}
	}
	@Override
	public void setView(AppInfo info) {
		ArrayList<SafeInfo> safeList = info.safe;
		
		SafeInfo safeInfo1 = safeList.get(0);
		ImageLoader.getInstance().displayImage(Url.HOME_IMAGE + safeInfo1.safeUrl, iv_image1, ImageLoaderOptions.pagerOptions);
		ImageLoader.getInstance().displayImage(Url.HOME_IMAGE + safeInfo1.safeDesUrl, iv_des_image1, ImageLoaderOptions.pagerOptions);
		tv_safe_des1.setText(safeInfo1.safeDes);
//		tv_safe_des1.setTextColor(safeInfo1.safeDesColor);//字体
		
		if (safeList.size()>1) {
			SafeInfo safeInfo2 = safeList.get(1);
			ImageLoader.getInstance().displayImage(Url.HOME_IMAGE + safeInfo2.safeUrl, iv_image2, ImageLoaderOptions.pagerOptions);
			ImageLoader.getInstance().displayImage(Url.HOME_IMAGE + safeInfo2.safeDesUrl, iv_des_image2, ImageLoaderOptions.pagerOptions);
			tv_safe_des2.setText(safeInfo2.safeDes);
		} else {
			ll_des2.setVisibility(View.GONE);
		}
		
		if (safeList.size()>2) {
			SafeInfo safeInfo3 = safeList.get(2);
			ImageLoader.getInstance().displayImage(Url.HOME_IMAGE + safeInfo3.safeUrl, iv_image3, ImageLoaderOptions.pagerOptions);
			ImageLoader.getInstance().displayImage(Url.HOME_IMAGE + safeInfo3.safeDesUrl, iv_des_image3, ImageLoaderOptions.pagerOptions);
			tv_safe_des3.setText(safeInfo3.safeDes);
		} else {
			ll_des3.setVisibility(View.GONE);
		}
	}

}
