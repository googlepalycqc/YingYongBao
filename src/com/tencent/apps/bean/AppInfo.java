package com.tencent.apps.bean;

import java.util.ArrayList;

public class AppInfo {
	// 一期的字段
	public int id;// app的唯一标识
	public String des;// app的描述信息
	public String downloadUrl;// apk文件的下载地址的后缀
	public String iconUrl;// app图标的url地址的后缀
	public String name;// app的名称
	public String packageName;// app的包名
	public long size;// apk的体积大小
	public float stars;// app的星级

	// 二期新增的字段
	public String author;// app的作者
	public String date;// app的上传日期
	public String downloadNum;// app的下载数量
	public String version;// app的当前版本
	public ArrayList<String> screen;// app的截图的url后缀
	 public ArrayList<SafeInfo> safe;//app的安全信息

}
