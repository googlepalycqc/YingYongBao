package com.tencent.apps.utils;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tencent.apps.bean.AppInfo;

public class GsonUtil {
/**
 * 1 json  ---> map<>   2 map<> --->json
 * 3 json  ---> list    4 list  ---> json 
 * 5 json  ---> bean    6 bean  ---> json
 *
 * */
	//json  ---> map<>  Type type = new TypeToken<HashMap<String, Object>>() {}.getType();
	public static Map<String, Object> jsonToMap(String json,Type type ) {
		if (json != null) {
			Gson gson = new Gson();
			 HashMap<String, Object> map = gson.fromJson(json, type);
			return map;
		} else {
			return null;
		}
	}
	
	//json  ---> list   new TypeToken<List<?>>() {}.getType()
	public static List<?>  jsonToList(String json,Type type){
		if (json != null) {
			Gson gson = new Gson();
			 List<?> list  = gson.fromJson(json, type);
			return list;
		} else {
			return null;
		}
	}

	
	//json  ---> list    
	public static<T> T jsonToBean(String json,Class<T> clazz){
		if (json != null) {
			Gson gson = new Gson();
			T t = gson.fromJson(json, clazz);
			return t;
		} else {
			return null;
		}
	}
	
	//map<> --->json
	public static String mapToJson(Map<String, Object> map){
		if (map != null) {
			Gson gson = new Gson();
			Type type = new TypeToken<HashMap<String, Object>>() {}.getType();
			String json = gson.toJson(gson, type);
			return json;
		} else {
			return null;
		}
	}
	
	// list  ---> json 
	public static String listToJson(List<?> list){
		if (list != null) {
			Gson gson = new Gson();
			Type type = new TypeToken<List<?>>() {}.getType();
			String json = gson.toJson(gson, type);
			return json;
		} else {
			return null;
		}
	}
	
	//bean  ---> json
	public static<T> String beanToJson(Class<T> clazz){
		if (clazz != null) {
			Gson gson = new Gson();
			String json = gson.toJson(clazz);
			return json;
		} else {
			return null;
		}
	}
}
