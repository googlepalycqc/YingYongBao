package com.tencent.apps.utils;

import java.util.Random;

import android.graphics.Color;
/**
 * 随机获取颜色
 * @author cui
 *
 */
public class ColorUtil {
	public static int getRandomColor(){
		Random random = new Random();
		// 如果颜色值过大会偏白，过小偏黑，所以需要设置中间的值
		int red = random.nextInt(150)+50;
		int green = random.nextInt(150)+50;
		int blue = random.nextInt(150)+50;
		int textColor = Color.rgb(red, green, blue);//根据rgb混色出一种新的颜色
		return textColor;
	}
}
