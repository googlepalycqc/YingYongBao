package com.tencent.apps.utils;

import android.util.Log;

public class LogUtil {
	private static boolean isDebug = true;//代码开发完毕，需要关闭
	public static String tag="LogUtil";
	
	public static void d(String msg){
		if(isDebug){
			Log.d(tag, msg);
		}
	}
	
	/**
	 * 打印d级别的log
	 * @param tag
	 * @param msg
	 */
	public static void d(String tag,String msg){
		if(isDebug){
			Log.d(tag, msg);
		}
	}
	/**
	 * 打印d级别的log
	 * @param tag
	 * @param msg
	 */
	public static void d(Object object,String msg){
		if(isDebug){
			Log.d(object.getClass().getSimpleName(), msg);
		}
	}
	
	/**
	 * 打印e级别的log
	 * @param tag
	 * @param msg
	 */
	public static void e(String tag,String msg){
		if(isDebug){
			Log.e(tag, msg);
		}
	}
	
	/**
	 * 打印e级别的log
	 * @param tag
	 * @param msg
	 */
	public static void e(Object object,String msg){
		if(isDebug){
			Log.e(object.getClass().getSimpleName(), msg);
		}
	}
}
