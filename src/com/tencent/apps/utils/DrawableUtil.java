package com.tencent.apps.utils;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;

public class DrawableUtil {
	
	/**
	 * 动态生成  圆角+有色+长方形  的图片
	 * @param argb
	 * @param radius
	 * @return
	 */
	public static GradientDrawable  generateDrawable(int argb,float radius){
		GradientDrawable drawable = new GradientDrawable();
		drawable.setColor(argb);
		drawable.setCornerRadius(radius);
		drawable.setShape(GradientDrawable.RECTANGLE);
		return drawable;
	}
	
	/**
	 * 动态生成selector
	 * @param normal
	 * @param pressed
	 * @return
	 */
	public static StateListDrawable generateSeletor(Drawable normal, Drawable pressed){
		StateListDrawable drawable = new StateListDrawable();
		drawable.addState(new int[]{android.R.attr.state_pressed},pressed);
		drawable.addState(new int[]{},normal);
		return drawable;
	}
}
