package com.tencent.apps.utils;

import com.tencent.apps.global.AppsApplication;

import android.widget.Toast;

public class ToastUtil {
	private static Toast toast;
	public static void showToast(String text){
		if(toast==null){
			toast = Toast.makeText( AppsApplication.getContext(), text, 0);
		}
		toast.setText(text);
		toast.show();
	}
}
