package com.tencent.apps.utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.tencent.apps.global.AppsApplication;
import com.tencent.apps.ui.fragment.ContentPager;

/*
 * 零散的工具
 */
public class CommonUtil {
	private static final String TAG = "CommonUtil";

	/**
	 * 正主线程执行runnable
	 * */
	public static void runOnUiThread(Runnable runnable) {
		AppsApplication.getmainHandler().post(runnable);
	}

	public static Resources getResources() {
		return AppsApplication.getContext().getResources();
	}

	public static int getColor(int resId) {
		return getResources().getColor(resId);
	}

	public static String getString(int resId) {
		return getResources().getString(resId);
	}

	public static String[] getStringArray(int resId) {
		return getResources().getStringArray(resId);
	}

	public static Drawable getDrawable(int resId) {
		return getResources().getDrawable(resId);
	}

	public static float getDimension(int resId) {
		return getResources().getDimension(resId);
	}

	//去除掉viewpaper中的contentPager
	public static void removeSelfFromParent(View child) {//注意：不是：ContentPager contentPager
		if (child != null) {
			ViewParent parent = child.getParent();
			LogUtil.d(TAG, "child != null");
			if (parent instanceof ViewGroup) {
				ViewGroup group = (ViewGroup) parent;
				group.removeView(child);//将子view从父view中移除
				LogUtil.d(TAG, "parent instanceof ViewGroup");
			} 
		}
	}
}
