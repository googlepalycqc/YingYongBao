package com.tencent.apps.global;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

/**
 * 提供全局变量 context + mainThreadHandler
 * */
public class AppsApplication extends Application {

	private static Context context;
	private static Handler mainHandler;

	@Override
	public void onCreate() {
		context=this;
		mainHandler = new Handler();//主线程的handler
		
		initImageLoader();
		super.onCreate();//放在第一行还是这里没区别
	}
	
	public void initImageLoader() {
		//imageLoader要在application的oncreate()方法中初始化
		//1 设置参数
		ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
		
		config.threadPriority(Thread.NORM_PRIORITY - 2);
		config.denyCacheImageMultipleSizesInMemory();//不会在内存中缓存多个大小的图片
		config.diskCacheFileNameGenerator(new Md5FileNameGenerator());//为了保证图片名称唯一
		config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
		//内存缓存大小默认是：app可用内存的1/8
		config.tasksProcessingOrder(QueueProcessingType.LIFO);
		config.writeDebugLogs(); // Remove for release app
		
		//2初始化
		ImageLoader.getInstance().init(config.build());
	}
	public static Context getContext(){
		return context;
	}
	public static Handler getmainHandler(){
		return mainHandler;
	}
}
