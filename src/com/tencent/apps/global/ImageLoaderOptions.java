package com.tencent.apps.global;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;

import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.tencent.apps.R;
/**
 * imageLoader的设置,从github事例中直接copy
 * @author cui
 *
 */
public class ImageLoaderOptions {
	//1 listView中图片参数
	public static  DisplayImageOptions options = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.ic_default)//加载中显示的图片
			.showImageForEmptyUri(R.drawable.ic_default)//图片url为空显示的图片
			.showImageOnFail(R.drawable.ic_default)//显示失败替代的图片
			.cacheInMemory(true)//在内存中缓存
			.cacheOnDisk(true)//在硬盘中缓存
			.considerExifParams(true)//识别图片的方向信息
//			.displayer(new FadeInBitmapDisplayer(3000))//图片渐变效果显示
			.displayer(new RoundedBitmapDisplayer(30)).build();//图片圆角显示：边角处內圆的半径值。  若想圆形显示，则设置>=imageview宽/2，且宽=高。[可直接给1000]
	

	//2 viewPager中参数设置()
	public static DisplayImageOptions pagerOptions = new DisplayImageOptions.Builder()
	
			.showImageForEmptyUri(R.drawable.ic_default)
			.showImageOnFail(R.drawable.ic_default)
			.resetViewBeforeLoading(true)//加载前清空imageview上的图片
			.cacheInMemory(true)
			.cacheOnDisk(true)
			.imageScaleType(ImageScaleType.EXACTLY)//图片的缩放类型，对图片进行进一步的压缩  EXACTLY_STRETCHED:拉伸
			.bitmapConfig(Config.RGB_565)//设置图片的色彩模式,是比较节省内存的模式
			.displayer(new FadeInBitmapDisplayer(2000))
//			.displayer(new RoundedBitmapDisplayer(1000)) //当imageview的height= wrap_content高可能显示不出来。
			.build();
			
			
}
